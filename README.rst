========================
Guestbook project
========================

Django проект, позволяющий создавать гостевые книги и оставлять в них комментарии.
Комментарии можно модерировать.

Local run
==========
Для запуска проекта на локальной машине нужно следовать следующим шагам:

#. Запуск vagrant::

    $ vagrant up && vagrant ssh

#. Создание схем баз данных::

    $ ./guestbook/manage.py migrate

#. Создание суперпользователя::

    $ ./guestbook/manage.py createsuperuser --username=admin --email=admin@yandex.ru

#. Запуск разработческого сервера::

    $ ./guestbook/manage.py runserver 0.0.0.0:8000

#. Открытие сайта на хостовой машине в браузере::

    http://localhost:8000/

Run the Tests
==============
Для запуска тестов нужно следовать следующим шагам:

#. Запуск тестов::

    $ make test

Follows Best Practices
======================

.. image:: http://twoscoops.smugmug.com/Two-Scoops-Press-Media-Kit/i-C8s5jkn/0/O/favicon-152.png
   :name: Two Scoops Logo
   :align: center
   :alt: Two Scoops of Django
   :target: http://twoscoopspress.org/products/two-scoops-of-django-1-6

Этот проект использует лучшие практики, изложенные в `Two Scoops of Django: Best Practices for Django 1.6`_.

.. _`Two Scoops of Django: Best Practices for Django 1.6`: http://twoscoopspress.org/products/two-scoops-of-django-1-6