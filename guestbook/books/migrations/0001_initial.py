# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import autoslug.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('title', models.CharField(verbose_name='название', max_length=50)),
                ('moderate', models.BooleanField(verbose_name='премодерация', default=True)),
                ('slug', autoslug.fields.AutoSlugField(editable=False, verbose_name='метка книги')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='владелец')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
