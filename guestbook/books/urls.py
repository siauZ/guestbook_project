from django.conf.urls import url, patterns

from . import views


urlpatterns = patterns("",
    url(
        regex=r'^$',
        view=views.BookListView.as_view(),
        name='list'
    ),
    url(
        regex=r'^(?P<slug>[\w.@+-]+)/$',
        view=views.BookDetailView.as_view(),
        name='detail'
    ),
    url(
        regex=r'^~create/$',
        view=views.BookCreateView.as_view(),
        name='create'
    ),
    url(
        regex=r'^(?P<pk>\d+)/~update/$',
        view=views.BookUpdateView.as_view(),
        name='update'
    ),
    url(
        regex=r'^(?P<pk>\d+)/~delete/$',
        view=views.BookDeleteView.as_view(),
        name='delete'
    ),
)