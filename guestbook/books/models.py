from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from autoslug import AutoSlugField
from django.db.models import QuerySet
from model_utils.managers import PassThroughManager


class BookQuerySet(QuerySet):
    def own(self, user):
        return self.filter(owner=user).order_by('-title')


class Book(models.Model):

    title = models.CharField('название', max_length=50)
    moderate = models.BooleanField('премодерация', default=True)
    slug = AutoSlugField(
        'метка книги',
        unique=True,
        populate_from= 'title'
    )

    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name='владелец'
    )

    objects = PassThroughManager.for_queryset_class(BookQuerySet)()

    def get_absolute_url(self):
        settings.AUTOSLUG_SLUGIFY_FUNCTION(self.owner)
        return reverse('books:detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title
