from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.db.models import Prefetch
from django.views.generic import ListView, UpdateView, CreateView, DeleteView, DetailView
from braces.views import LoginRequiredMixin, SelectRelatedMixin, PrefetchRelatedMixin

from .models import Book
from comments.models import Comment


class BookListView(LoginRequiredMixin, SelectRelatedMixin, ListView):
    model = Book
    select_related = ('owner', )

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.own(self.request.user)


class BookDetailView(PrefetchRelatedMixin, DetailView):
    model = Book
    prefetch_related = (
        Prefetch('comment_set', queryset=Comment.objects.public().select_related('author')),
    )

class BookActionMixin(SuccessMessageMixin):

    model = Book
    fields = ('title', 'moderate')
    success_url = reverse_lazy('books:list')


class BookCreateView(BookActionMixin, LoginRequiredMixin, CreateView):
    success_message = 'Вы создали новую книгу'

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class BookUpdateView(BookActionMixin, LoginRequiredMixin, UpdateView):
    success_message = 'Вы изменили книгу'


class BookDeleteView(LoginRequiredMixin, DeleteView):
    model = Book
    success_url = reverse_lazy('books:list')