import factory

from .. import models


class BookFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Book

    title = 'книга_test'
    owner = factory.SubFactory('core.factories.UserFactory')