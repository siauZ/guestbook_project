from django.test import TestCase
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from books.tests.factories import BookFactory

from ..views import BookCreateView, BookListView, BookUpdateView, BookDeleteView
from ..models import Book
from core.factories import FakeRequestFactory


class BookTest(TestCase):
    def setUp(self):
        self.factory = FakeRequestFactory()
        self.factory.login()

    def test_book_list_view(self):
        my_book = BookFactory(owner=self.factory.user)
        BookFactory()

        request = self.factory.get(reverse('books:list'))
        book_list_view = BookListView.as_view()

        response = book_list_view(request)
        self.assertEqual(response.status_code, 200)
        self.assertCountEqual(response.context_data['book_list'], [my_book])

    def test_redirect_book_create_view(self):
        new_book_data = {
            'title': 'новая_книга_test'
        }
        request = self.factory.post(reverse('books:create'), new_book_data)
        book_create_view = BookCreateView.as_view()

        response = book_create_view(request)
        self.assertEqual(response.status_code, 302)
        self.assertIsInstance(response, HttpResponseRedirect)
        self.assertEqual(response.url, reverse('books:list'))

    def test_book_create_view(self):
        new_book_data = {
            'title': 'новая_книга_test'
        }
        request = self.factory.post(reverse('books:create'), new_book_data)
        book_create_view = BookCreateView.as_view()

        book_create_view(request)
        self.assertEqual(Book.objects.first().owner, request.user)

    def test_book_update_view(self):
        book = BookFactory(owner=self.factory.user)
        update_book_data = {
            'title': 'измененная_книга_test'
        }
        request = self.factory.post(reverse('books:update', kwargs={'pk': book.pk}), update_book_data)
        book_update_view = BookUpdateView.as_view()

        book_update_view(request, pk=book.pk)
        self.assertEqual(Book.objects.first().title, update_book_data['title'])

    def test_book_delete_view(self):
        book = BookFactory(owner=self.factory.user)
        request = self.factory.post(reverse('books:delete', kwargs={'pk': book.pk}))
        book_delete_view = BookDeleteView.as_view()

        book_delete_view(request, pk=book.pk)
        self.assertFalse(Book.objects.all())