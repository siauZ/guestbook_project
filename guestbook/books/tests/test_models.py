from django.test import TestCase

from ..models import Book
from .factories import BookFactory


class BookTest(TestCase):

    def setUp(self):
        self.book = BookFactory()

    def test_book_get_absolute_url(self):
        url = '/books/{}/'.format(self.book.slug)
        self.assertEqual(self.book.get_absolute_url(), url)

    def test_book_str(self):
        self.assertEqual(self.book.__str__(), 'книга_test')


class BookQuerySetTest(TestCase):

    def test_book_get_absolute_url(self):
        my_book = BookFactory()
        BookFactory()

        my_books = Book.objects.own(my_book.owner)
        self.assertEqual(len(my_books), 1)
        self.assertEqual(my_books[0], my_book)