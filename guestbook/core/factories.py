import factory
from django.test import RequestFactory
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth import get_user_model


class AnonymousUserFactory(AnonymousUser):
    pass


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = get_user_model()

    username = factory.Sequence(lambda n: 'user_test{}'.format(n))

    email = factory.LazyAttribute(lambda a: '{}@example.com'.format(a.username).lower())
    password = factory.PostGenerationMethodCall('set_password', 'defaultpassword')


class FakeMessages:
    """
    Мок Django message framework, использования сообщений в тестах
    """
    messages = []

    def add(self, level, message, extra_tags):
        self.messages.append(str(message))

    @property
    def pop(self):
        return self.messages.pop()


class FakeRequestFactory(RequestFactory):
    """
    Фабрика запросов, поддерживающая автроизцаию пользователя
    и работу с сообщениями
    """

    def __init__(self, **defaults):
        super().__init__(**defaults)
        self.user = AnonymousUserFactory()

    def login(self, user=None):
        self.user = user or UserFactory()
        self.user.is_authenticated = lambda: True

    def generic(self, *args, **kwargs):
        request = super().generic(*args, **kwargs)
        request.user = self.user
        request._messages = FakeMessages()

        return request
