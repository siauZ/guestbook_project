from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import CreateView

from .models import Comment


class CommentCreateView(SuccessMessageMixin, CreateView):
    model = Comment
    fields =  ('content', 'book')
    success_message = 'Вы добавили комментарий!'

    def form_valid(self, form):
        if self.request.user.is_authenticated():
            form.instance.author = self.request.user
        return super(CommentCreateView, self).form_valid(form)

    def get_success_url(self):
        return self.object.book.get_absolute_url()
