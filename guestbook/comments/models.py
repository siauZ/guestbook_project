from django.conf import settings
from django.db import models
from django.db.models import Q, QuerySet
from model_utils.managers import PassThroughManager
from model_utils.models import TimeStampedModel


class CommentQuerySet(QuerySet):
    def public(self):
        return self.filter(Q(book__moderate=True, checked=True) | Q(book__moderate=False)).order_by('-created')


class Comment(TimeStampedModel):

    content = models.CharField('содержание', max_length=140)
    checked = models.BooleanField('проверено', default=False)

    book = models.ForeignKey(
        'books.Book',
        verbose_name='гостевая книга'
    )
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name='автор',
        null=True,
        blank=True
    )

    objects = PassThroughManager.for_queryset_class(CommentQuerySet)()

    def __str__(self):
        return self.content
