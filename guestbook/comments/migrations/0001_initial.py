# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import model_utils.fields
from django.conf import settings
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('books', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(verbose_name='created', editable=False, default=django.utils.timezone.now)),
                ('modified', model_utils.fields.AutoLastModifiedField(verbose_name='modified', editable=False, default=django.utils.timezone.now)),
                ('content', models.CharField(verbose_name='содержание', max_length=140)),
                ('checked', models.BooleanField(verbose_name='проверено', default=False)),
                ('author', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, verbose_name='автор')),
                ('book', models.ForeignKey(to='books.Book', verbose_name='гостевая книга')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
