from django.test import TestCase
from django.test.client import RequestFactory
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from ..views import CommentCreateView
from ..models import Comment
from books.tests.factories import BookFactory
from core.factories import FakeRequestFactory


class CommentTest(TestCase):

    def setUp(self):
        self.factory = FakeRequestFactory()
        self.book = BookFactory()

    def test_redirect_comment_create_view(self):
        new_comment_data = {
            'book': self.book.pk,
            'content': 'комментарий_test'
        }
        request = self.factory.post(reverse('comments:create'), new_comment_data)
        comment_create_view = CommentCreateView.as_view()

        response = comment_create_view(request)
        self.assertEqual(response.status_code, 302)
        self.assertIsInstance(response, HttpResponseRedirect)
        self.assertEqual(response.url, self.book.get_absolute_url())

    def test_anonymous_comment_create_view(self):
        new_comment_data = {
            'book': self.book.pk,
            'content': 'комментарий_test'
        }
        request = self.factory.post(reverse('comments:create'), new_comment_data, follow=True)
        comment_create_view = CommentCreateView.as_view()

        comment_create_view(request)
        self.assertIsNone(Comment.objects.first().author)

    def test_has_author_comment_create_view(self):
        new_comment_data = {
            'book': self.book.pk,
            'content': 'комментарий_test'
        }
        self.factory.login()
        request = self.factory.post(reverse('comments:create'), new_comment_data, follow=True)
        comment_create_view = CommentCreateView.as_view()

        comment_create_view(request)
        self.assertEqual(Comment.objects.first().author, request.user)
