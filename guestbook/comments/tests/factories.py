import factory

from .. import models


class CommentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Comment

    content = 'комментарий_test'
    book = factory.SubFactory('books.tests.factories.BookFactory')