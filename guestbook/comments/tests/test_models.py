from django.test import TestCase

from .factories import CommentFactory
from ..models import Comment


class CommentTest(TestCase):

    def setUp(self):
        self.comment = CommentFactory()

    def test_comment_str(self):
        self.assertEqual(self.comment.__str__(), 'комментарий_test')


class CommentQuerySetTest(TestCase):

    def test_show_all_comment(self):
        comment = CommentFactory(book__moderate=False)

        public_comment = Comment.objects.filter(book=comment.book).public()
        self.assertCountEqual(public_comment, [comment])

    def test_show_checked_comment(self):
        comment = CommentFactory(book__moderate=True, checked=True)
        CommentFactory(book=comment.book)

        public_comment = Comment.objects.filter(book=comment.book).public()
        self.assertCountEqual(public_comment, [comment])