from django.conf.urls import url, patterns

from . import views


urlpatterns = patterns("",
    url(
        regex=r'^create/$',
        view=views.CommentCreateView.as_view(),
        name='create'
    ),
)