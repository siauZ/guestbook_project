var mCreateUpdate = $('#create-update'),
    mCreateUpdateForm = mCreateUpdate.find('form'),
    mCreateUpdateTitle = mCreateUpdate.find('input[name="title"]'),
    mCreateUpdateModerate = mCreateUpdate.find('input[name="moderate"]');

$('a[href="#create-update"]').on('click', function () {
    var link = $(this),
        url = link.data('url'),
        title = '',
        moderate = true;

    if (link.hasClass('badge')) {
        var parent = link.parent();
        title = parent.find('.name').text();
        if (parent.find('.label-warning').length == 0) {
            moderate = false;
        }
    }

    mCreateUpdateForm.attr('action', url);
    mCreateUpdateTitle.val(title);
    mCreateUpdateModerate.prop('checked', moderate);

    mCreateUpdate.modal('show');

    return false;
});

var mDelete = $('#delete'),
    mDeleteForm = mDelete.find('form');

$('a[href="#delete"]').on('click', function () {
    var link = $(this),
        url = link.data('url');

    mDeleteForm.attr('action', url);

    mDelete.modal('show');

    return false;
});

$('button.s-approve-all').on('click', function () {
    var form = $('form.moderate'),
        checkbox = form.find('input[name="moderate"]');

    $.each(checkbox, function () {
        $(this).prop('checked', true);
    })
});