from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
    url(
        r'^$',
        RedirectView.as_view(url=reverse_lazy('books:list')),
        name='index'
    ),
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login', name='logout'),
    url(r'^books/', include('books.urls', namespace='books')),
    url(r'^comments/', include('comments.urls', namespace='comments')),

    url(r'^admin/', include(admin.site.urls)),

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
