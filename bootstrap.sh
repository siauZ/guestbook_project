#!/usr/bin/env bash

# название проекта
PRJ_NAME=$1

# версия Python
PY_VERSION="3.3"
# версия PostgreSQL
PG_VERSION=9.3

apt-get update -y
# настройка репозиториев

if ! command -v add-apt-repository
then
    apt-get -y install python-software-properties
fi

add-apt-repository ppa:fkrull/deadsnakes # python

PG_REPO_APT_SOURCE=/etc/apt/sources.list.d/pgdg.list # postgresql
if [ ! -f "$PG_REPO_APT_SOURCE" ]
then
  echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > "$PG_REPO_APT_SOURCE"
  wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | apt-key add -
fi

apt-get update 2>&1 >/dev/null

# gcc and psycopg2
apt-get -y install python$PY_VERSION-dev libevent-dev libpq-dev libjpeg-dev

# make
apt-get -y install make

# locale
locale-gen ru_RU.UTF-8
dpkg-reconfigure locales
update-locale LANG=ru_RU.UTF-8 LC_ALL=ru_RU.UTF-8

# postgresql
PG_CONF="/etc/postgresql/$PG_VERSION/main/postgresql.conf"
PG_HBA="/etc/postgresql/$PG_VERSION/main/pg_hba.conf"
PG_DIR="/var/lib/postgresql/$PG_VERSION/main"

APP_DB_USER=dev
APP_DB_PASS=dbpass
APP_DB_NAME=db_local

if ! command -v psql
then
  apt-get -y install "postgresql-$PG_VERSION" "postgresql-contrib-$PG_VERSION"

    # Edit postgresql.conf to change listen address to "*":
    sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "$PG_CONF"

    # Append to pg_hba.conf to add password auth:
    echo "host    all             all             all                     md5" >> "$PG_HBA"

    # Restart so that all new config is loaded:
    service postgresql restart

    cat << EOF | su - postgres -c psql
    -- Create the database user:
    CREATE USER $APP_DB_USER
        WITH PASSWORD '$APP_DB_PASS' SUPERUSER CREATEDB;

    -- Create the database:
    CREATE DATABASE $APP_DB_NAME
        WITH OWNER $APP_DB_USER
        TEMPLATE=template0
        ENCODING = 'UTF-8'
        LC_COLLATE = 'ru_RU.UTF-8'
        LC_CTYPE = 'ru_RU.UTF-8';
EOF
fi


# python
if ! command -v python$PY_VERSION
then
    apt-get -y install python$PY_VERSION
fi

# pip
if ! command -v pip
then
    wget https://bootstrap.pypa.io/ez_setup.py -O - | sudo python$PY_VERSION
    easy_install pip
fi

# virtualenv
PROJECT_HOME="/home/vagrant"
WORKON_HOME="${PROJECT_HOME}/.virtualenvs"
VIRTUALENVWRAPPER_PYTHON="/usr/bin/python$PY_VERSION"

if ! command -v mkvirtualenv
then
    pip install virtualenvwrapper
    echo -e "
    # virtualenvwrapper settings
    export WORKON_HOME=$WORKON_HOME
    export PROJECT_HOME=$PROJECT_HOME
    export VIRTUALENVWRAPPER_PYTHON=$VIRTUALENVWRAPPER_PYTHON
    source /usr/local/bin/virtualenvwrapper.sh
    workon $PRJ_NAME
    "  >> $PROJECT_HOME/.bashrc

    su vagrant 
    source /usr/local/bin/virtualenvwrapper.sh
    mkvirtualenv -a $PROJECT_HOME/${PRJ_NAME}_project -r $PROJECT_HOME/${PRJ_NAME}_project/requirements/local.txt $PRJ_NAME
fi