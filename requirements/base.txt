# Bleeding edge Django
Django==1.7.7

# Configuration
django-configurations==0.8
dj-database-url==0.3.0

# Models
django-model-utils==2.2

# ViewMixins
django-braces==1.4.0


# For the persistence stores
psycopg2==2.6

# Unicode slugification
unicode-slugify==0.1.3
django-autoslug==1.7.2

# Useful things
bpython==0.14.1

