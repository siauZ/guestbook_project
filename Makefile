# Some helpful utility commands.
test:
	rm -rf ./htmlcov
	coverage run --source=./guestbook --omit=*/tests*,*/urls.py,*/migrations*,*/config*,*/wsgi.py ./guestbook/manage.py test guestbook --settings=config --configuration=Local
	coverage html -d ./htmlcov